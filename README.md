# Plantilla para inicio de sesión con gestión de usuarios por parte del administrador #

### ¿Cómo funciona?
Al Administrador inicial se le proporciona una clave y un usuario, que se crean diréctamente
en la base de datos.

Accede poniendo sus datos en la pantalla de login, luego en el menubar accede a Administrador/
panel de administrador. Desde ahí puede gestionar usuarios y realizar distintas tareas que se
integren en esa pantalla dependiendo de las exigencias del cliente.

### ¿Qué hay integrado en la aplicación?
Necesitarás la base de datos, la cual solo contiene una tabla para usuarios, también necesitarás
las librerías de Hibernate. Un fichero de configuración con las siguientes opciones:
hibernate.connection.username=user
hibernate.connection.password=password
hibernate.connection.url=jdbc:mysql://localhost/nombre_base
Este fichero se encuentra en C:\Usuarios\nombre\arasync_inicio_sesion.config.

### Observaciones:
El inicio no contiene ningún tipo de seguridad más allá de una comparación de Strings. No se almacena
la contraseña una vez que se cierra el programa (en la base de datos obviamente, si). Esto no debería
ser un problema por que no interesa que se guarde la contraseña en un negocio. 
La idea de esta plantilla es irla mejorando a lo largo del tiempo pero mantener la esencia y usarla a 
menudo para empezar nuevos proyectos y ahorrarnos tiempo.

### Herramientas utilizadas
JavaFX, Scene Builder, Intellij

