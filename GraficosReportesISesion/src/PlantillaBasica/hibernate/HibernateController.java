package PlantillaBasica.hibernate;

import PlantillaBasica.iniciosesion.entidades.Usuarios;
import javafx.scene.control.Alert;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;
import org.hibernate.exception.JDBCConnectionException;
import org.hibernate.exception.SQLGrammarException;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import org.hibernate.service.spi.ServiceException;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import static PlantillaBasica.util.VariablesGlobales.RUTA_FICHERO;

/**
 * Created by Juan on 12/05/2017.
 */
public class HibernateController {
    private static SessionFactory sessionFactory;
    public static Session session;

    public void conectar() {
        Properties prop = new Properties();
        InputStream in = null;

        try {
            in = new FileInputStream(RUTA_FICHERO);
            prop.load(in);

        } catch (FileNotFoundException e) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Fichero no encontrado");
            alert.setHeaderText("Debe crear primero el fichero de conexión en configuración");
            alert.showAndWait();
            System.exit(0);

        } catch (IOException e) {

        }

        prop.getProperty("hibernate.connection.username");
        prop.getProperty("hibernate.connection.password");
        prop.getProperty("hibernate.connection.url");

        try {
            Configuration configuration = new Configuration();
            configuration.configure().
                    setProperties(prop).
                    configure("hibernate.cfg.xml");

            ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.setProperties(prop).
                    getProperties()).buildServiceRegistry();
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);

            session = sessionFactory.openSession();

        }catch (ServiceException ms){
            ms.printStackTrace();

        } catch (HibernateException e) {
            e.printStackTrace();
        }
    }

    public static Session getCurrentSession() {
        if (!session.isOpen())
            session = sessionFactory.openSession();

        return session;
    }

    public static boolean poll() {
        boolean connected = true;
        try {
            final Session session = getCurrentSession();
            final Transaction tx = session.beginTransaction();
            tx.commit();
            session.close();
        } catch (JDBCConnectionException ex) {
            connected = false;
        } catch (SQLGrammarException ge){
            connected = false;
        }

        return connected;
    }

    public ArrayList<Usuarios> getUsuarios() {
        Query query = getCurrentSession().
                createQuery("FROM Usuarios ORDER BY usuario");
        ArrayList<Usuarios> usuarios = (ArrayList<Usuarios>) query.list();
        return usuarios;
    }

    public void insertarUsuario(Usuarios usuarios){
        Session session = getCurrentSession();
        session.beginTransaction();
        session.save(usuarios);
        session.getTransaction().commit();
        session.close();
    }

    public void eliminarUsuario(Usuarios usuarios){
        Session session = getCurrentSession();
        session.beginTransaction();
        session.delete(usuarios);
        session.getTransaction().commit();
        session.close();
    }

    public void modificarUsuario(Usuarios usuarios){
        Session session = getCurrentSession();
        session.beginTransaction();
        session.update(usuarios);
        session.getTransaction().commit();
        session.close();
    }
}
