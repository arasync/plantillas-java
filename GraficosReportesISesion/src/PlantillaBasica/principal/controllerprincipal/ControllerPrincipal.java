package PlantillaBasica.principal.controllerprincipal;

import PlantillaBasica.hibernate.HibernateController;
import PlantillaBasica.iniciosesion.entidades.Usuarios;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.swing.JRViewer;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * Created by Juan on 13/05/2017.
 */
public class ControllerPrincipal implements Initializable {
    HibernateController hibernateController = new HibernateController();
    private ObservableList data;
    private Connection conexion;

    @FXML
    private Button btVerTodoGraficos;
    @FXML
    private ListView lvGraficos;
    @FXML
    private AnchorPane panelGraficos;
    @FXML
    private AnchorPane panelAdministracion;
    @FXML
    private AnchorPane panelInicial;
    @FXML
    private MenuItem miAdministracion;
    @FXML
    private MenuItem miInicio;
    @FXML
    private MenuItem miCerrar;
    @FXML
    private MenuItem miGraficos;

    @FXML
    private TableView tvSesion;
    @FXML
    private TableColumn colSesionUsuario;
    @FXML
    private TableColumn colPasswordSesion;
    @FXML
    private TableColumn colRolSesion;
    @FXML
    private TextField tfUsuarioAdministracion;
    @FXML
    private TextField tfPasswordAdministracion;
    @FXML
    private ComboBox cbRolAdministracion;
    @FXML
    private Button btAgregarAdministracion;
    @FXML
    private Button btEliminarAdministracion;
    @FXML
    private Button btModificarAdministracion;

    @FXML
    private Button btCrearInforme;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        rellenarListaGraficos();
        rellenarTablaSesion();
        rellenarComboRol();
        btVerTodoGraficos.setOnAction(event -> verTodoGraficos());
        btCrearInforme.setOnAction(event -> crearInforme());
        btModificarAdministracion.setOnAction(event -> modificarUsuario());
        btEliminarAdministracion.setOnAction(event -> eliminarUsuario());
        btAgregarAdministracion.setOnAction(event -> insertarUsuario());
        miAdministracion.setOnAction(event -> irAdministracion());
        miInicio.setOnAction(event -> irInicio());
        miCerrar.setOnAction(event -> System.exit(0));
        miGraficos.setOnAction(event -> irGraficos());

        lvGraficos.getSelectionModel().select(null);
        lvGraficos.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (lvGraficos.getSelectionModel().getSelectedItems() != null) {
                Usuarios filaseleccionada = (Usuarios) lvGraficos.getSelectionModel().getSelectedItem();
                if (filaseleccionada != null) {
                    System.out.println(filaseleccionada.getUsuario());
                }
            }
        });

        tvSesion.getSelectionModel().select(null);
        tvSesion.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (tvSesion.getSelectionModel().getSelectedItem() != null) {
                Usuarios filaseleccionada = (Usuarios) tvSesion.getSelectionModel().getSelectedItem();
                tfUsuarioAdministracion.setText(filaseleccionada.getUsuario());
                tfPasswordAdministracion.setText(filaseleccionada.getPassword());
                String rol = filaseleccionada.getRol();
                cbRolAdministracion.setValue(rol);
            }
        });

        irInicio();

    }

    private void verTodoGraficos() {
        lvGraficos.getSelectionModel().clearSelection();

    }


    private void rellenarTablaSesion() {
        data = getInitialTableData();
        tvSesion.setItems(data);
        colSesionUsuario.setCellValueFactory(new PropertyValueFactory("usuario"));
        colPasswordSesion.setCellValueFactory(new PropertyValueFactory("password"));
        colRolSesion.setCellValueFactory(new PropertyValueFactory("rol"));
        tvSesion.getColumns().setAll(colSesionUsuario, colPasswordSesion, colRolSesion);
    }

    private void rellenarListaGraficos() {
        data = getInitialTableData();
        lvGraficos.cellFactoryProperty();
        lvGraficos.setItems(data);

        lvGraficos.setCellFactory(param -> new ListCell<Usuarios>() {
            @Override
            protected void updateItem(Usuarios item, boolean empty) {
                super.updateItem(item, empty);

                if (empty || item == null || item.getUsuario() == null) {
                    setText(null);
                } else {
                    setText(item.getUsuario());
                }
            }
        });
    }

    private ObservableList getInitialTableData() {
        ObservableList data = FXCollections.observableList(hibernateController.getUsuarios());
        return data;
    }

    private void rellenarComboRol() {
        ArrayList listaCombo = new ArrayList();
        listaCombo.add("Administrador");
        listaCombo.add("Usuario");
        cbRolAdministracion.setItems(FXCollections.observableArrayList(listaCombo));
        cbRolAdministracion.setValue(null);

    }

    private void insertarUsuario() {
        String usuario = tfUsuarioAdministracion.getText();
        String password = tfPasswordAdministracion.getText();
        String rol = String.valueOf(cbRolAdministracion.getValue());

        if (usuario.equalsIgnoreCase("") || password.equalsIgnoreCase("") ||
                cbRolAdministracion.getValue() == null) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Error enviando datos");
            alert.setHeaderText("Hay campos sin rellenar");
            alert.setContentText("Por favor, rellena todos los campos");
            alert.showAndWait();
        }

        Usuarios usuarios = new Usuarios();
        usuarios.setUsuario(usuario);
        usuarios.setPassword(password);
        usuarios.setRol(rol);

        hibernateController.insertarUsuario(usuarios);
        vaciarFormInsertar();
        rellenarTablaSesion();

    }

    private void eliminarUsuario() {
        Usuarios filaseleccionada = (Usuarios) tvSesion.getSelectionModel().getSelectedItem();
        int numeroFila = tvSesion.getSelectionModel().getFocusedIndex();

        if (numeroFila >= 0) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Eliminar");
            alert.setHeaderText("Eliminar usuario");
            alert.setContentText("¿Estás seguro?");

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                hibernateController.eliminarUsuario(filaseleccionada);
                rellenarTablaSesion();
                vaciarFormInsertar();
            } else {
                return;
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Sin selección");
            alert.setHeaderText("No has seleccionado ningún usuario");
            alert.setContentText("Por favor, selecciona un usuario en la lista.");
            alert.showAndWait();
        }
    }

    private void modificarUsuario() {
        Usuarios usuarios = (Usuarios) tvSesion.getSelectionModel().getSelectedItem();
        int numeroFila = tvSesion.getSelectionModel().getSelectedIndex();

        if (numeroFila >= 0) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Modificar");
            alert.setHeaderText("Modificar usuario");
            alert.setContentText("¿Estás seguro?");

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                usuarios.setUsuario(tfUsuarioAdministracion.getText());
                usuarios.setPassword(tfPasswordAdministracion.getText());
                usuarios.setRol(String.valueOf(cbRolAdministracion.getValue()));

                hibernateController.modificarUsuario(usuarios);
                vaciarFormInsertar();
                rellenarTablaSesion();
            } else {
                return;
            }

        } else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Sin selección");
            alert.setHeaderText("No has seleccionado ningún usuario");
            alert.setContentText("Por favor, selecciona un usuario en la lista.");
            alert.showAndWait();
        }
    }

    private void vaciarFormInsertar() {
        tfUsuarioAdministracion.setText("");
        tfPasswordAdministracion.setText("");
        cbRolAdministracion.setValue(null);
    }

    private void irAdministracion() {
        panelAdministracion.setVisible(true);
        panelInicial.setVisible(false);
        panelGraficos.setVisible(false);

        panelAdministracion.setOpacity(1);
        panelInicial.setOpacity(0);
        panelGraficos.setOpacity(0);
    }

    private void irInicio() {
        panelInicial.setVisible(true);
        panelAdministracion.setVisible(false);
        panelGraficos.setVisible(false);

        panelInicial.setOpacity(1);
        panelAdministracion.setOpacity(0);
        panelGraficos.setOpacity(0);
    }

    private void irGraficos() {
        panelGraficos.setVisible(true);
        panelInicial.setVisible(false);
        panelAdministracion.setVisible(false);

        panelGraficos.setOpacity(1);
        panelInicial.setOpacity(0);
        panelAdministracion.setOpacity(0);

    }

    private void crearInforme() {
        conexion = null;
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/inicio_sesion", "root", "");
            JasperReport report = (JasperReport)
                    JRLoader.loadObject(this.getClass().getClassLoader().getResourceAsStream("recursos/reportes/inicio_sesion.jasper"));
            JasperPrint jasperPrint = JasperFillManager.fillReport(report,
                    null, conexion);

            JRViewer jrViewer = new JRViewer(jasperPrint);
            JFrame frame = new JFrame();
            frame.setSize(800, 600);
            frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
            frame.setContentPane(jrViewer);
            frame.setLocationRelativeTo(null);
            frame.setTitle("Título para el informe");
            frame.setVisible(true);

        } catch (JRException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
