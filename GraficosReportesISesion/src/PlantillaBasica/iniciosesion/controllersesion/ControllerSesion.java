package PlantillaBasica.iniciosesion.controllersesion;

import PlantillaBasica.Main;
import PlantillaBasica.iniciosesion.entidades.Usuarios;
import PlantillaBasica.hibernate.HibernateController;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ControllerSesion implements Initializable {
    private HibernateController hibernateControlador;

    @FXML
    private MenuItem menuInicioCerrar;
    @FXML
    private TextField tfUsuarioEnviar;
    @FXML
    private PasswordField tfPasswordEnviar;
    @FXML
    private Button btEnviarInicio;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        conectar();

        menuInicioCerrar.setOnAction(event -> System.exit(0));
        btEnviarInicio.setOnAction(event -> enviarInicioSesion());
    }

    private void enviarInicioSesion() {
        String user;
        String pass;

        user = tfUsuarioEnviar.getText();
        pass = tfPasswordEnviar.getText();

        if (user.equalsIgnoreCase("") || pass.equalsIgnoreCase("")) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Error iniciando sesión");
            alert.setHeaderText("Hay campos sin rellenar");
            alert.setContentText("Por favor, rellena todos los campos");
            alert.showAndWait();
            return;
        }

        for (Usuarios usuarios : hibernateControlador.getUsuarios()) {
            if (usuarios.getUsuario().equalsIgnoreCase(user) && usuarios.getPassword().equalsIgnoreCase(pass)) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Bienvenido!");
                alert.setHeaderText("Bienvenido " + usuarios.getUsuario());
                alert.setContentText("Tienes acceso a las funciones de " + usuarios.getRol());
                alert.showAndWait();

                try {
                    Stage stage = (Stage) btEnviarInicio.getScene().getWindow();
                    Parent root = FXMLLoader.load(Main.class.getResource("/recursos/ventanas/ventanaprincipal.fxml"));
                    Scene scene = new Scene(root, 860, 600);
                    scene.getStylesheets().add(Main.class.getResource("/recursos/css/estilos.css").toExternalForm());
                    stage.setScene(scene);
                    stage.show();
                    return;

                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
        }

        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Error iniciando sesión");
        alert.setHeaderText("Los campos no coinciden");
        alert.setContentText("Comprueba que los campos están bien escritos");
        alert.showAndWait();
    }


    //    Conexión con la base de datos
    private void conectar() {
        hibernateControlador = new HibernateController();
        hibernateControlador.conectar();

        if (hibernateControlador.poll() == false) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Conexión");
            alert.setHeaderText("Error de conexión");
            alert.setContentText("Comprueba el fichero de configuración o revise el apartado de conexión en el manual de usuario.");
            alert.showAndWait();
            System.exit(0);
        } else {
            //Rellenar tablas y demás
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Conexión");
            alert.setHeaderText("Conectado con éxito");
            alert.showAndWait();
        }
    }
}
